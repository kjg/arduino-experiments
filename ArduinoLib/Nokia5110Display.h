/* 
* Nokia5110Display.h
*
* Created: 23.06.2014 23:56:23
* Author: gerk
*/

#ifndef __NOKIA5110DISPLAY_H__
#define __NOKIA5110DISPLAY_H__

/*
  Hardware: (Note most of these pins can be swapped)
  Graphic LCD Pin ---------- Arduino Pin
  1-VCC       ----------------  5V
  2-GND       ----------------  GND
  3-SCE       ----------------  7
  4-RST       ----------------  6
  5-D/C       ----------------  5
  6-DN(MOSI)  ----------------  11
  7-SCLK      ----------------  13
  8-LED       - 330 Ohm res --  9
  The SCLK, DN(MOSI), must remain where they are, but the other
  pins can be swapped. The LED pin should remain a PWM-capable
  pin. Don't forget to stick a current-limiting resistor in line
  between the LCD's LED pin and Arduino pin 9!
*/
#include <Arduino.h>

#include "Nokia5110Ascii.h"

/* PCD8544-specific defines: */
#define NOKIA5110_LCD_COMMAND  0
#define NOKIA5110_LCD_DATA     1

/* 84x48 LCD Defines: */
#define NOKIA5110_LCD_WIDTH   84 // Note: x-coordinates go wide
#define NOKIA5110_LCD_HEIGHT  48 // Note: y-coordinates go high
#define NOKIA5110_DISPLAY_BUFFER_SIZE (NOKIA5110_LCD_WIDTH * NOKIA5110_LCD_HEIGHT / 8)

#define NOKIA5110_WHITE       0  // For drawing pixels. A 0 draws white.
#define NOKIA5110_BLACK       1  // A 1 draws black.

class Nokia5110Display
{
//variables
public:
protected:

	int scePin;    // SCE - Chip select, pin 3 on LCD.
	int rstPin;    // RST - Reset, pin 4 on LCD.
	int dcPin;     // DC - Data/Command, pin 5 on LCD.
	int sdinPin;   // DN(MOSI) - Serial data, pin 6 on LCD.
	int sclkPin;   // SCLK - Serial clock, pin 7 on LCD.
	int blPin;     // LED - Backlight LED, pin 8 on LCD.

	Nokia5110Ascii ascii;

private:

//functions
public:
	Nokia5110Display();
	~Nokia5110Display();

	void SetBacklight(int intensity);

	void SetPixel(int x, int y);
	void ClearPixel(int x, int y);
	void SetPixel(int x, int y, boolean bw);

	void DrawText(int x, int y, const char* text, boolean bw);
	void DrawCharacter(int x, int y, char character, boolean bw);

	void CopyFromBuffer(const byte *buffer);
	
	void Invert();
	void Flush();

protected:
	void LcdBegin();
	inline void LcdWrite(byte data_or_command, byte data);
	void SetContrast(int contrast);
	
	void GotoXY(int x, int y);
	
private:
	Nokia5110Display( const Nokia5110Display &c );
	Nokia5110Display& operator=( const Nokia5110Display &c );

}; //Nokia5110Display

#endif //__NOKIA5110DISPLAY_H__
