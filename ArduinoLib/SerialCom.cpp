/* 
* SerialInterface.cpp
*
* Created: 15.08.2014 00:30:15
* Author: gerk
*/

#include <Arduino.h>
#include "SerialCom.h"

// default constructor
ArduinoLib::SerialCom::SerialCom()
{
} //SerialInterface

// default destructor
ArduinoLib::SerialCom::~SerialCom()
{
} //~SerialInterface

void ArduinoLib::SerialCom::Begin(int bps)
{
	Serial.begin(bps);
}

bool ArduinoLib::SerialCom::IsReady()
{
	return true;
}

size_t ArduinoLib::SerialCom::BytesAvailable()
{
	return Serial.available();
}

int ArduinoLib::SerialCom::ReadByte()
{
	return Serial.read();
}

size_t ArduinoLib::SerialCom::ReadBytesUntil(char terminator, char *buffer, size_t length)
{
	return Serial.readBytesUntil(terminator, buffer, length);
}

size_t ArduinoLib::SerialCom::Write(char* data)
{
	return Serial.write(data);
}

size_t ArduinoLib::SerialCom::Write(int data)
{
	return Serial.write(data);
}

size_t ArduinoLib::SerialCom::WriteLine(char* data)
{
	return Serial.write(data) + Serial.write(NEWLINE);
}
