/* 
* SerialCom.h
*
* Created: 15.08.2014 00:30:15
* Author: gerk
*/

#ifndef __ARDUINO_LIB_SERIALCOM_H__
#define __ARDUINO_LIB_SERIALCOM_H__

#include <McCommonLib.h>

namespace ArduinoLib {
	
	class SerialCom : McCommonLib::ISerialCom
	{
		//variables
		public:
		protected:
		private:

		//functions
		public:
		SerialCom();
		~SerialCom();

		void Begin(int bps);

		/**
		 * @return True when serial interface is ready
		 */
		bool IsReady();
		/**
		 * @return Number of bytes available for read
		 */
		size_t BytesAvailable();
		/**
		 * The first byte of incoming serial or -1 if no data is available
		 *
		 * @return the next byte 
		 */
		int ReadByte();
		/**
		 * Read zero terminated string
		 *
		 * @return number of bytes read
		 */
		size_t ReadBytesUntil(char terminator, char *buffer, size_t length);
		/**
		 * Write data from null-terminated string
		 *
		 * @return the number of bytes written
		 */
		size_t Write(char* data);
		/**
		 * Write integer
		 *
		 * @return number of bytes written
		 */
		size_t Write(int data);
		/**
		 * Write string and a newline
		 *
		 * @return the number of bytes written
		 */
		size_t WriteLine(char* data);
		
		protected:
		private:
		SerialCom( const SerialCom &c );
		SerialCom& operator=( const SerialCom &c );


	}; //SerialInterface

} // namespace

#endif //__ARDUINO_LIB_SERIALCOM_H__
