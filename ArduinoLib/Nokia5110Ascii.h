/* 
* Nokia5110Ascii.h
*
* Created: 25.06.2014 11:05:40
* Author: gerk
*/


#ifndef __NOKIA5110ASCII_H__
#define __NOKIA5110ASCII_H__

#include <Arduino.h>

class Nokia5110Ascii
{
//variables
public:
protected:
private:

//functions
public:
	Nokia5110Ascii();
	~Nokia5110Ascii();

	const byte* GetData(char c);
protected:
private:
	Nokia5110Ascii( const Nokia5110Ascii &c );
	Nokia5110Ascii& operator=( const Nokia5110Ascii &c );

}; //Nokia5110Ascii

#endif //__NOKIA5110ASCII_H__
