    
#include "Arduino.h"

#include <Nokia5110Display.h>

#include "GameOfLife.h"
	 
Nokia5110Display display;	 
GameOfLife gameOfLife(NOKIA5110_LCD_WIDTH, NOKIA5110_LCD_HEIGHT);
     
void setup()
{
	Serial.begin(9600);
	
	gameOfLife.Place8x8Pattern(40, 20, GOL_DoubleHorseShoe);
}
     
void loop()
{
	if (random(0, 1000) > 970) {

		int x = random(10, 74);
		int y = random(10, 38);
		
		gameOfLife.Place8x8Pattern(x, y, GOL_DoubleHorseShoe);
	}
	
	gameOfLife.StepFast();
	
	display.CopyFromBuffer(gameOfLife.GetCells());
	display.Flush();	 
	 
	static int backlight = -90;
	display.SetBacklight(round((sin(backlight * 0.017f) + 1) * 64) + 1);
	backlight += 3;
}