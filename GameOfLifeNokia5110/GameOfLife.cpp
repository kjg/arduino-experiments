/* 
* GameOfLife.cpp
*
* Created: 24.06.2014 11:46:14
* Author: gerk
*/

#include <string.h>
#include "GameOfLife.h"

static byte __gameOfLifePatterns[][8] = {
	{	// 0 - double horse shoe
		0b00000111,
		0b00000101,
		0b00000101,
		0b00000000,
		0b00000101,
		0b00000101,
		0b00000111,
		0b00000000,
	}
};

// default constructor
GameOfLife::GameOfLife(int width, int height)
{
	this->width = width;
	this->height = height;
	
	int numBytes = width * height / 8;
	
	cells = new byte[numBytes];
	memset(cells, 0, numBytes);
	tempCells = new byte[numBytes];
	memset(tempCells, 0, numBytes);
} //GameOfLife

// default destructor
GameOfLife::~GameOfLife()
{
	delete(cells);
	delete(tempCells);
} //~GameOfLife

int GameOfLife::GetWidth()
{
	return width;
}

int GameOfLife::GetHeight()
{
	return height;
}

const byte* GameOfLife::GetCells()
{	
	return cells;
}

void GameOfLife::Step()
{
	memset(tempCells, 0, width * height / 8);
	
	for (int y = 1; y < height - 1; y++)
	{
		for (int x = 1; x < width - 1; x++)
		{
			int numNeighbors =
				GetCell(x-1, y-1) + GetCell(x  , y-1) + GetCell(x+1, y-1) +
				GetCell(x-1, y  ) +                     GetCell(x+1, y  ) +
				GetCell(x-1, y+1) + GetCell(x  , y+1) + GetCell(x+1, y+1);
				
			byte currentCell = GetCell(x, y);
			
			if (numNeighbors == 3 || (numNeighbors == 2 && currentCell)) {
				SetCellOnBuffer(tempCells, x, y);
			}			
		}
	}
	
	memcpy(cells, tempCells, width * height / 8);
}

void GameOfLife::StepFast()
{
	memset(tempCells, 0, width * height / 8);
	
	for (byte y = 1; y < height - 1; y++)
	{
		byte i11, i21, i31,
			 i12, i22, i32,
			 i13, i23, i33;
		
		i11 = GetCell(0, y-1); i21 = GetCell(1, y-1);
		i12 = GetCell(0, y  ); i22 = GetCell(1, y  );
		i13 = GetCell(0, y+1); i23 = GetCell(1, y+1);
		
		for (byte x = 1; x < width - 1; x++)
		{
			i31 = GetCell(x+1, y-1);
			i32 = GetCell(x+1, y  );
			i33 = GetCell(x+1, y+1);
			
			byte numNeighbors =
				i11 + i21 + i31 +
				i12 +       i32 +
				i13 + i23 + i33
			;

			if (numNeighbors == 3 || (numNeighbors == 2 && i22 == 1)) {
				SetCellOnBuffer(tempCells, x, y);
			}
			
			i11 = i21; i21 = i31;
			i12 = i22; i22 = i32;
			i13 = i23; i23 = i33;
		}
	}
	
	memcpy(cells, tempCells, width * height / 8);
}

void GameOfLife::SetCell(int x, int y)
{
	SetCellOnBuffer(cells, x, y);
}

void GameOfLife::ClearCell(int x, int y)
{
	byte shift = y & 0x07;  // equals y % 8

	cells[x + (y >> 3) * width] &= ~(1<<shift);
}

byte GameOfLife::GetCell(int x, int y)
{
	byte shift = y & 0x07;  // equals y % 8

	return cells[x + (y >> 3) * width] & (1<<shift) ? 1 : 0;
}

void GameOfLife::Place8x8Pattern(int x, int y, GameOfLifePatterns pattern)
{
	const byte* data = __gameOfLifePatterns[pattern];
	Place8x8Pattern(x, y, data);
}
	
void GameOfLife::Place8x8Pattern(int x, int y, const byte * data) 
{	
	for (int dy = 0; dy < 8; dy++)
	{
		byte currentByte = data[dy];
		
		for (int dx = 0; dx < 8; dx++)
		{
			byte mask = 128 >> dx;
			
			if (currentByte & mask) {
				SetCell(x + dx, y + dy);
			}
		}
	}
}

void GameOfLife::SetCellOnBuffer(byte* buf, int x, int y)
{
	byte shift = y & 0x07;  // equals y % 8

	buf[x + (y >> 3) * width] |= 1<<shift;
}

void GameOfLife::ClearCellOnBuffer(byte* buf, int x, int y)
{
	byte shift = y & 0x07;  // equals y % 8

	buf[x + (y >> 3) * width] &= ~(1<<shift);
}
