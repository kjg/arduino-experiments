/* 
* GameOfLife.h
*
* Created: 24.06.2014 11:46:15
* Author: gerk
*/


#ifndef __GAMEOFLIFE_H__
#define __GAMEOFLIFE_H__

typedef unsigned char byte;

#define GAMEOFLIFE_ALIVE 1
#define GAMEOFLIFE_DEAD  0

enum GameOfLifePatterns
{
	GOL_DoubleHorseShoe = 0,	
};

class GameOfLife
{
//variables
public:
protected:
private:
	int width;
	int height;

	byte* cells;
	byte* tempCells;

//functions
public:
	GameOfLife(int width, int height);
	~GameOfLife();
	
	int GetWidth();
	int GetHeight();
	
	const byte* GetCells();
	
	void Step();
	void StepFast();
	
	void SetCell(int x, int y);
	void ClearCell(int x, int y);
	inline byte GetCell(int x, int y);
	
	void Place8x8Pattern(int x, int y, GameOfLifePatterns pattern);
	void Place8x8Pattern(int x, int y, const byte* data);
	
protected:
private:
	GameOfLife( const GameOfLife &c );
	GameOfLife& operator=( const GameOfLife &c );
	
	inline void SetCellOnBuffer(byte* buf, int x, int y);
	inline void ClearCellOnBuffer(byte* buf, int x, int y);
	
}; //GameOfLife

#endif //__GAMEOFLIFE_H__
