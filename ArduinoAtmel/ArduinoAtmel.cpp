
// SEE http://www.engblaze.com/tutorial-using-atmel-studio-6-with-arduino-projects/

#include "Arduino.h"

#include <Nokia5110Display.h>
#include <Nokia5110Ascii.h>

void setup();
void loop();

Nokia5110Display display;

void setup()
{
	Serial.begin(9600);

	display.Flush();
}

void loop()
{
	display.DrawText(random(0, 80), random(0,40), "Hello You", random(0, 2));
//	display.Invert();

	display.Flush();

	static int backlight = -90;
	display.SetBacklight(round((sin(backlight++ * 0.017f) + 1) * 64));
}