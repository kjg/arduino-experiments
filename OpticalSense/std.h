/*
 * std.h
 *
 * Created: 15.08.2014 17:17:16
 *  Author: gerk
 */ 


#ifndef STD_H_
#define STD_H_

#include <string.h>

#include "McCommonLib.h"

#define PRODUCT "PRODUCT:OpticalSense\nVERSION:0.1\nVENDOR:Graul Industries\nIS_IT_FUN:true"

typedef enum {
	READY = 0,
	CALIBRATING = 1,
	MEASUREING = 2
} Status_t; 

#endif /* STD_H_ */