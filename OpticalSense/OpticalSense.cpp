#include "std.h"

using namespace McCommonLib;

#ifdef ARDUINO
	
    #include "Arduino.h"

    #include <ArduinoLib.h>

	ArduinoLib::SerialCom serial;

    void setup()
    {
		serial.Begin(9600);
    }

#endif	
	
Status_t status = READY;	
	
void loop()
{
	if (serial.BytesAvailable()) {
		char input[64];
		memset(input, 0, sizeof(input));
		serial.ReadBytesUntil(0, input, sizeof(input) - 1);
		
		StringUtil::ToUpper(input);

		if (StringUtil::StartsWith(input, "ID")) {
			serial.WriteLine((char*) PRODUCT);
			serial.Write(0);
		} else if (StringUtil::StartsWith(input, "PING")) {
			serial.Write((char*) "PONG");
			serial.Write(0);
		} else if (StringUtil::StartsWith(input, "STATUS")) {
			serial.Write((char*) "STATUS:");
			serial.Write(status + 48); // int to char
			serial.Write((char*) NEWLINE);
			serial.Write(0);
		}
	}
}
