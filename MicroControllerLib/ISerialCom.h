/* 
* ISerialInterface.h
*
* Created: 14.08.2014 23:46:36
* Author: gerk
*/


#ifndef __ISERIALINTERFACE_H__
#define __ISERIALINTERFACE_H__

namespace McCommonLib {
		
	class ISerialCom
	{
		//functions
		public:

		virtual void Begin(int bps) = 0;

		/**
		 * @return True when serial interface is ready
		 */
		virtual bool IsReady() = 0;
		/**
		 * @return Number of bytes available for read
		 */
		virtual size_t BytesAvailable() = 0;
		/**
		 * The first byte of incoming serial or -1 if no data is available
		 *
		 * @return the next byte 
		 */
		virtual int ReadByte() = 0;
		/**
		 * Read zero terminated string
		 *
		 * @return the next byte 
		 */
		virtual size_t ReadBytesUntil(char terminator, char *buffer, size_t length);
		/**
		 * Write string
		 *
		 * @return number of bytes written
		 */
		virtual size_t Write(char* data);
		/**
		 * Write integer
		 *
		 * @return number of bytes written
		 */
		virtual size_t Write(int data);
		/**
		 * Write string and a newline
		 *
		 * @return the number of bytes written
		 */
		virtual size_t WriteLine(char* data);

	}; //ISerialInterface

} // namespace
#endif //__ISERIALINTERFACE_H__
