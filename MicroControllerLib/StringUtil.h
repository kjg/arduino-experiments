/* 
* StringUtil.h
*
* Created: 15.08.2014 17:00:03
* Author: gerk
*/


#ifndef __STRINGUTIL_H__
#define __STRINGUTIL_H__

namespace McCommonLib {
	
	class StringUtil
	{
		//functions
		public:
		/**
		 * compare strings case insensitive
		 */
		static int CmpNoCase(const char *a, const char *b);

		/**
		 * check if subject starts with given prefix
		 *
		 * @return bool
		 */
		static bool StartsWith(const char *subject, const char *prefix);

		static void ToLower(char *a);

		static void ToUpper(char *a);

	}; //StringUtil
}

#endif //__STRINGUTIL_H__
