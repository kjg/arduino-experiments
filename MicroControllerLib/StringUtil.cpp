/* 
* StringUtil.cpp
*
* Created: 15.08.2014 17:00:03
* Author: gerk
*/

#include <ctype.h>
#include <string.h>
#include "StringUtil.h"

// functions
int McCommonLib::StringUtil::CmpNoCase(const char *a, const char *b)
{
	for (;; a++, b++) {
		int d = tolower(*a) - tolower(*b);
		if (d != 0 || !*a)
		return d;
	}
}

bool McCommonLib::StringUtil::StartsWith(const char *subject, const char *prefix)
{	
	size_t lenSubject = strlen(subject);
	size_t lenPrefix = strlen(prefix);
	return lenSubject < lenPrefix ? false : strncmp(subject, prefix, lenPrefix) == 0;	
}

void McCommonLib::StringUtil::ToLower(char *a)
{
	for ( ; *a; ++a) *a = tolower(*a);
}

void McCommonLib::StringUtil::ToUpper(char *a)
{
	for ( ; *a; ++a) *a = toupper(*a);
}